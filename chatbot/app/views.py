from django.shortcuts import render
import nltk
import numpy as np
import string
import warnings
warnings.filterwarnings("ignore")
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import cosine_similarity
import requests
import json
import random
import subprocess
from subprocess import Popen
import time
import sys
f = open('C:/Users/andri/Downloads/chatbot/chatbot/app/content.txt','r',errors = 'ignore', encoding = 'utf-8')
paragraph = f.read()
nltk.download('punkt')   # for first-time use only. Punkt is a Sentence Tokenizer
nltk.download('wordnet')    # for first-time use only. WordNet is a large lexical database of English.
sent_tokens = nltk.sent_tokenize(paragraph)
word_tokens = nltk.word_tokenize(paragraph)

greetings = ['Hey','Hallo', 'Hello', 'Hi', 'It\'s great to see you', 'Nice to see you']
bye = ['Bye', 'Bye-Bye', 'Goodbye', 'Have a good day',"terimakaish telah berkunjung", "semoga harimu menyenangkan"]
thank_you = ['Thanks', 'Thank you', 'Thanks a bunch', 'Thanks a lot.', 'Thank you very much', 'Thanks so much', 'Thank you so much',"Happy to help!", "Any time!", "My pleasure"]
thank_response = ['You\'re welcome.' , 'No problem.', 'No worries.', ' My pleasure.' , 'It was the least I could do.', 'Glad to help.']

def bot_initialize(user_msg):
    if(user_response in greetings):
          return random.choice(greetings)
    elif(user_response in thank_you):
          return random.choice(thank_response)


lemmer = nltk.stem.WordNetLemmatizer()
def LemTokens(tokens):
    return [lemmer.lemmatize(token) for token in tokens]
remove_punct_dict = dict((ord(punct), None) for punct in string.punctuation)
def Normalize(text):
    return LemTokens(nltk.word_tokenize(text.lower().translate(remove_punct_dict)))

def response(user_response):
    robo_response = ''
    
    sent_tokens.append(user_response)   # Appending the Question user ask to sent_tokens to find the Tf-Idf and cosine_similarity between User query and the content.
    TfidfVec = TfidfVectorizer(tokenizer = Normalize, stop_words='english')    #tokenizer ask about Pre-processing parameter and it will consume the Normalize() function and it will also remove StopWords
    tfidf = TfidfVec.fit_transform(sent_tokens)
    vals = cosine_similarity(tfidf[-1], tfidf)     # It will do cosine_similarity between last vectors and all the vectors because last vector contain the User query
    idx = vals.argsort()[0][-2]     # argsort() will sort the tf_idf in ascending order. [-2] means second last index i.e. index of second highest value after sorting the cosine_similarity. Index of last element is not taken as query is added at end and it will have the cosine_similarity with itself.
    flat = vals.flatten()    # [[0,...,0.89,1]] -> [0,...,0.89,1] this will make a single list of vals which had list inside a list.
    flat.sort()
    req_tfidf = flat[-2]  # this contains tfid value of second highest cosine_similarity
    if(req_tfidf == 0):    # 0 means there is no similarity between the question and answer
        robo_response = robo_response + "I am sorry! I don't understand you. Please rephrase your query."
        return robo_response
    
    else:
        robo_response = robo_response + sent_tokens[idx]    # return the sentences at index -2 as answer
        return robo_response

def bot_initialize(user_msg):
    flag=True
    while(flag==True):
        user_response = user_msg
        if(user_response not in bye):
            if(user_response == '/start'):
                bot_resp = "Hi! There. I am MusicInformation Bot. I can tell you all the Facts about Music. \nType Bye to Exit." 
                return bot_resp
            elif(user_response in thank_you):
                bot_resp = random.choice(thank_response)
                return bot_resp
            elif(user_response in greetings):
                bot_resp = random.choice(greetings) + ", I have a lot of information about music. how can i help you?"
                return bot_resp
            else:
                user_response = user_response.lower()
                bot_resp = response(user_response)
                sent_tokens.remove(user_response)
                return bot_resp
        else:
            flag = False
            bot_resp = random.choice(bye)
            return bot_resp


class telegram_bot():
    def __init__(self):
        self.token = "2011440931:AAFSJamiGFobZAplKJAMOARjU8hMmaOQBuI"
        self.url = f"https://api.telegram.org/bot{self.token}"
    def get_updates(self,offset=None):
        url = self.url+"/getUpdates?timeout=100"
        if offset:
            url = url+f"&offset={offset+1}"
        url_info = requests.get(url)
        return json.loads(url_info.content)
    def send_message(self,msg,chat_id):
        url = self.url + f"/sendMessage?chat_id={chat_id}&text={msg}"
        if msg is not None:
            requests.get(url)
    def grab_token(self):
        return tokens

def make_reply(msg):
	if msg is not None:
		reply = bot_initialize(msg)
	return reply
       
def index(request):
    return render(request, 'index.html', {
        'bot_message': "Hi! There. I am MusicInformation Bot. I can tell you all the Facts about Music. \nType Bye to Exit." ,
        'display': "none"
    })

def submit(request):
	t = 10
	while t:
		mins, secs = divmod(t,60)
		timer = '{:02d}:{:02d}'.format(mins, secs)
		time.sleep(1)
		t-=1
	if request.method == 'POST' :
		user_message = request.POST['user_message']
		user_message = user_message.capitalize()
		reply = make_reply(user_message)
		return render(request, 'index.html', {'bot_message': reply, 'user_message': user_message, 'display': "block"})
	return render(request,'index.html',{'bot_message':"thank you for contacting me. Call me if you need me again.",'display':"block"})


from threading import Thread
def bots():
	tbot = telegram_bot()
	update_id = None
	while True:
		print("...")
		updates = tbot.get_updates(offset=update_id)
		updates = updates['result']
		print(updates)
		if updates:
			for item in updates:
				update_id = item["update_id"]
				print(update_id)
				try:
					message = item["message"]["text"]
					message = message.capitalize()
					print(message)
				except:
					message = None
				from_ = item["message"]["from"]["id"]
				print(from_)
				reply = make_reply(message)
				tbot.send_message(reply,from_)

Thread(target = bots).start() 